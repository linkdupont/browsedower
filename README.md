# Browsedower

Browsedower lets you choose the browser to open a URL.

# Installation

```
mkdir build
cd build
cmake ..
ninja build
ninja install
```

# Usage

Set Browsedower as your default web browser. Browsedower will show a window
listing all installed browsers, allowing you to pick the browser you want to
open the URL.
