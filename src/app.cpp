/*
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  SPDX-FileCopyrightText: 2022 Link Dupont <link@sub-pop.net>
 */

#include "app.h"

#include <KIO/ApplicationLauncherJob>
#include <KIO/JobUiDelegateFactory>
#include <KService>
#include <KSharedConfig>
#include <KWindowConfig>
#include <QQuickWindow>
#include <QGuiApplication>

Q_LOGGING_CATEGORY(browsedower, "browsedower")

void App::restoreWindowGeometry(QQuickWindow *window, const QString &group) const
{
    KConfig dataResource(QStringLiteral("data"), KConfig::SimpleConfig, QStandardPaths::AppDataLocation);
    KConfigGroup windowGroup(&dataResource, QStringLiteral("Window-") + group);
    KWindowConfig::restoreWindowSize(window, windowGroup);
    KWindowConfig::restoreWindowPosition(window, windowGroup);
}

void App::saveWindowGeometry(QQuickWindow *window, const QString &group) const
{
    KConfig dataResource(QStringLiteral("data"), KConfig::SimpleConfig, QStandardPaths::AppDataLocation);
    KConfigGroup windowGroup(&dataResource, QStringLiteral("Window-") + group);
    KWindowConfig::saveWindowPosition(window, windowGroup);
    KWindowConfig::saveWindowSize(window, windowGroup);
    dataResource.sync();
}

void App::launch(const QString storageId)
{
    qCDebug(browsedower) << storageId;
    auto ptr = KService::serviceByStorageId(storageId);
    auto job = new KIO::ApplicationLauncherJob(ptr);
    QList<QUrl> urls;
    if (!m_url.isEmpty()) {
        urls.append(m_url);
    }
    qCDebug(browsedower) << urls;
    job->setUrls(urls);
    connect(job, &KJob::result, this, &App::onResult);
    job->setUiDelegate(KIO::createDefaultJobUiDelegate(KJobUiDelegate::AutoHandlingEnabled, nullptr));
    job->start();
}

void App::onResult(KJob *job)
{
    if (job->error()) {
        qCDebug(browsedower) << job->errorText();
    }
    QCoreApplication::exit(0);
}

QUrl App::url() const
{
    return m_url;
}

void App::setUrl ( QUrl newUrl )
{
    if (m_url != newUrl) {
        m_url = newUrl;
        emit urlChanged(newUrl);
    }
}
