/*
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  SPDX-FileCopyrightText: 2022 Link Dupont <link@sub-pop.net>
 */

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.browsedower 1.0

Kirigami.ApplicationWindow {
    id: root

    title: i18n("browsedower")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    onClosing: App.saveWindowGeometry(root)

    onWidthChanged: saveWindowGeometryTimer.restart()
    onHeightChanged: saveWindowGeometryTimer.restart()
    onXChanged: saveWindowGeometryTimer.restart()
    onYChanged: saveWindowGeometryTimer.restart()

    Component.onCompleted: App.restoreWindowGeometry(root)

    // This timer allows to batch update the window size change to reduce
    // the io load and also work around the fact that x/y/width/height are
    // changed when loading the page and overwrite the saved geometry from
    // the previous session.
    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }

    property int counter: 0

    globalDrawer: Kirigami.GlobalDrawer {
        title: i18n("browsedower")
        titleIcon: "applications-graphics"
        isMenu: !root.isMobile
        actions: [
            Kirigami.Action {
                text: i18n("About browsedower")
                icon.name: "help-about"
                onTriggered: pageStack.layers.push('qrc:About.qml')
            },
            Kirigami.Action {
                text: i18n("Quit")
                icon.name: "application-exit"
                onTriggered: Qt.quit()
            }
        ]
    }

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: page

    Kirigami.Page {
        id: page

        Layout.fillWidth: true

        title: i18n("Main Page")
        header: Controls.ToolBar {
            RowLayout {
                anchors.fill: parent
                Controls.TextField {
                    Layout.fillWidth: true
                    text: App.url
                }
            }
        }

        ColumnLayout {
            id: layout
            anchors.fill: parent

            Kirigami.CardsLayout {
                id: cardsLayout

                Layout.alignment: Qt.AlignHCenter

                maximumColumns: 0

                Repeater {
                    model: BrowserList

                    delegate: Kirigami.AbstractCard {
                        showClickFeedback: true
                        contentItem: Item {
                            ColumnLayout {
                                anchors.fill: parent
                                Kirigami.Icon {
                                    Layout.alignment: Qt.AlignHCenter
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    source: iconName
                                    fallback: "internet-web-browser"
                                }
                                Controls.Label {
                                    Layout.alignment: Qt.AlignHCenter
                                    text: name
                                }
                            }
                        }
                        onClicked: {
                            App.launch(storageId);
                        }
                    }
                }
            }
        }
    }
}
