/*
    SPDX-License-Identifier: GPL-2.0-or-later
    SPDX-FileCopyrightText: 2022 Link Dupont <link@sub-pop.net>
*/

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>

#include "browserlist.h"
#include "about.h"
#include "app.h"
#include "version-browsedower.h"
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "browsedowerconfig.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setApplicationName(QStringLiteral("browsedower"));

    KAboutData aboutData(
                         // The program name used internally.
                         QStringLiteral("browsedower"),
                         // A displayable program name string.
                         i18nc("@title", "Browsedower"),
                         // The program version string.
                         QStringLiteral(BROWSEDOWER_VERSION_STRING),
                         // Short description of what the app does.
                         i18n("Routes URLs to desired browsers"),
                         // The license this code is released under.
                         KAboutLicense::GPL,
                         // Copyright Statement.
                         i18n("(c) 2022"));
    aboutData.addAuthor(i18nc("@info:credit", "Link Dupont}"),
                        i18nc("@info:credit", "Author Role"),
                        QStringLiteral("link@sub-pop.net"),
                        QStringLiteral("https://sub-pop.net"));
    KAboutData::setApplicationData(aboutData);

    QQmlApplicationEngine engine;

    auto config = browsedowerConfig::self();

    qmlRegisterSingletonInstance("org.kde.browsedower", 1, 0, "Config", config);

    AboutType about;
    qmlRegisterSingletonInstance("org.kde.browsedower", 1, 0, "AboutType", &about);

    App application;
    qmlRegisterSingletonInstance("org.kde.browsedower", 1, 0, "App", &application);

    BrowserList browserList;
    qmlRegisterSingletonInstance("org.kde.browsedower", 1, 0, "BrowserList", &browserList);

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    QCommandLineParser parser;
    parser.process(app);
    const QStringList args = parser.positionalArguments();
    if (args.count() > 0) {
        application.setUrl(QUrl(args.at(0)));
    }

    return app.exec();
}
