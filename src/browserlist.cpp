/*
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  SPDX-FileCopyrightText: 2022 Link Dupont <link@sub-pop.net>
 */

#include "browserlist.h"

#include <KApplicationTrader>
#include <KService>

Q_LOGGING_CATEGORY(browserList, "browsedower.browserList")

BrowserList::BrowserList()
{
    KService::List list = KApplicationTrader::queryByMimeType("x-scheme-handler/http");

    for (int i = 0; i < list.size(); ++i) {
        list[i].detach();
        KService *item = list[i].take();
        qCDebug(browserList) << "Found http handler:" << item->name();
        m_browsers.append(item);
    }
}


QVariant BrowserList::data ( const QModelIndex& index, int role ) const
{
    if (!index.isValid()) {
        return {};
    }

    if (index.row() > m_browsers.count()) {
        return {};
    }

    const KService *browser = m_browsers[index.row()];

    switch (role) {
        case NameRole:
            return QVariant::fromValue(browser->name());
        case IconNameRole:
            return QVariant::fromValue(browser->icon());
        case StorageIdRole:
            return QVariant::fromValue(browser->storageId());
        default:
            return {};
    }
}

int BrowserList::rowCount ( const QModelIndex& parent ) const
{
    if (parent.isValid()) {
        return 0;
    }
    return m_browsers.count();
}

QHash< int, QByteArray > BrowserList::roleNames() const
{
    return {{NameRole, "name"}, {IconNameRole, "iconName"}, {StorageIdRole, "storageId"}};
}
