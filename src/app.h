/*
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  SPDX-FileCopyrightText: 2022 Link Dupont <link@sub-pop.net>
 */

#pragma once

#include <QObject>
#include <QUrl>
#include <QLoggingCategory>

class BrowserItem;
class QQuickWindow;

class KJob;
class KService;

Q_DECLARE_LOGGING_CATEGORY(browsedower)

class App : public QObject
{
    Q_OBJECT
    Q_PROPERTY (QUrl url READ url WRITE setUrl NOTIFY urlChanged);
public:
    // Restore current window geometry
    Q_INVOKABLE void restoreWindowGeometry(QQuickWindow *window, const QString &group = QStringLiteral("main")) const;
    // Save current window geometry
    Q_INVOKABLE void saveWindowGeometry(QQuickWindow *window, const QString &group = QStringLiteral("main")) const;

    QUrl url() const;

    Q_INVOKABLE void launch(const QString storageId);

public Q_SLOTS:
    void setUrl(QUrl newUrl);

Q_SIGNALS:
    void urlChanged(const QUrl newUrl);

private Q_SLOTS:
    void onResult(KJob *job);

private:
    QUrl m_url;
};
