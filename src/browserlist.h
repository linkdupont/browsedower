/*
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  SPDX-FileCopyrightText: 2022 Link Dupont <link@sub-pop.net>
 */

#pragma once

#include <QAbstractListModel>
#include <QLoggingCategory>

class KService;
class QByteArray;
class QModelIndex;

Q_DECLARE_LOGGING_CATEGORY(browserList)


/**
 * @todo write docs
 */
class BrowserList : public QAbstractListModel
{
    Q_OBJECT

public:
    BrowserList();
    enum BrowerListRoles {
        NameRole = Qt::UserRole + 1,
        IconNameRole,
        StorageIdRole
    };
    QVariant data ( const QModelIndex& index, int role ) const override;
    int rowCount ( const QModelIndex& parent ) const override;
    QHash< int, QByteArray > roleNames() const override;

private:
    QList<KService *> m_browsers;
};
